# install essentials
yay -S neovim python-neovim ruby-neovim tig vim-plug
yay -S tmux tmuxp ranger zathura zathura-pdf-mupdf
yay -S gvfs stow fzf man man-pages unzip unrar diff-so-fancy duf pandoc traceroute
yay -S ttf-ancient-fonts ttf-firacode-nerd ttf-liberation lib32-fontconfig lib32-systemd xdg-user-dirs
yay -S ruby ruby-build rubygems jre8-openjdk python-gitpython python-gitlab rxvt-unicode
yay -S zsh zsh-autosuggestions oh-my-zsh-git
yay -S ly
yay -S libreoffice dolphin ffmpegthumbs
yay -S syncthing syncthingtray
yay -S brave-bin cronie
yay -S flameshot zathura zathura-pdf-mupdf conky libreoffice ffmpegthumbs cronie libreoffice
yay -S mpv feh yt-dlp get_iplayer pulseaudio atomicparsely
yay -S wine filelight banner
yay -S pandoc xdg-user-dirs traceroute ntfs-3g mpv yt-dlp get_iplayer feh jq wget rbenv
yay -S xorg xorg-init i3 rofi dmenu polybar i3exit i3lock dex

chsh -s /usr/bin/zsh
git config --global core.pager "diff-so-fancy | less --tabs=4 -RFX"

# Other
cp after.README ~/after.README

xdg-user-dirs-update

git config --global core.pager "diff-so-fancy | less --tabs=4 -RFX"

