export VISUAL='nvim'
export EDITOR='nvim'
export BROWSER='/usr/bin/brave'

export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"

export HISTFILE="$HOME/.zhistory"
export HISTSIZE=10000
export SAVEHIST=10000

export DOTFILES="$HOME/.apps/dotfiles"
export ZSH_CONFIG_DIR="$XDG_CONFIG_HOME/zsh"

export ORG_DIR="$HOME/.apps/org"
export TMUXP_CONFIGDIR="$ORG_DIR/tmuxp"

export TERM='termite'
export TERMINAL='termite'

export GEM_HOME="$HOME/.gem"
export GEM_PATH="$HOME/.gem"
export PATH="$PATH:$GEM_PATH/ruby/3.0.0/bin"
eval "$(rbenv init -)"

export GPODDER_HOME=$HOME/.apps/data/gPodder
export RANGER_LOAD_DEFAULT_RD="False"


for file in $ORG_DIR/zsh_scripts/*.sh; do
	source "$file"
done
