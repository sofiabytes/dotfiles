ftmuxp(){
	if [[ -n $TMUX ]]; then
		return
	fi
	#Get IDs
	ID="$(ls $TMUXP_CONFIGDIR | sed -e 's/\.yml$//')"
	if [[ -z "$ID" ]]; then
		tmux new-session
	fi

	create_new_session="Create New Session"

	ID="${create_new_session}\n$ID"
	ID="$(echo $ID | fzf | cut -d: -f1)"

	if [[ "$ID" = "${create_new_session}" ]]; then
		tmux new-session
	elif [[ -n "$ID" ]]; then
		echo "${ID%.*}"
		tmuxp load "${ID%.*}"
	fi
}
