# Env
export EDITOR=/usr/bin/nvim
export BROWSER=/usr/bin/brave
export GPODDER_HOME=$HOME/apps/data/gPodder

# Ruby
export PATH="$HOME/.rbenv/bin:$PATH"
eval "$(rbenv init -)"
